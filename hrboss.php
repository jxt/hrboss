<?php

require_once 'includes/functions.php';

$template           = isset($_REQUEST['selectTemplate']) ? trim($_REQUEST['selectTemplate']) : NULL;
$clientName         = isset($_REQUEST['clientName']) ? trim($_REQUEST['clientName']) : NULL;
$clientUrl          = isset($_REQUEST['clientUrl']) ? trim($_REQUEST['clientUrl']) : NULL;
$hrBossContactName  = isset($_REQUEST['hrBossContactName']) ? trim($_REQUEST['hrBossContactName']) : NULL;
$hrBossEmail        = isset($_REQUEST['hrBossEmail']) ? trim($_REQUEST['hrBossEmail']) : NULL;
$hrBossPhone        = isset($_REQUEST['hrBossPhone']) ? trim($_REQUEST['hrBossPhone']) : NULL;
$requiredFromClient = isset($_REQUEST['requiredFromClient']) ? trim($_REQUEST['requiredFromClient']) : NULL;
$clientFont         = isset($_REQUEST['clientFont']) ? trim($_REQUEST['clientFont']) : NULL;
$primaryColor       = isset($_REQUEST['primaryColor']) ? trim($_REQUEST['primaryColor']) : NULL;
$secondaryColor     = isset($_REQUEST['secondaryColor']) ? trim($_REQUEST['secondaryColor']) : NULL;
$brandingGuidelines = isset($_REQUEST['brandingGuidelines']) ? trim($_REQUEST['brandingGuidelines']) : NULL;
$selectLanguage     = isset($_REQUEST['selectLanguage']) ? trim($_REQUEST['selectLanguage']) : NULL;

//if (isset($_FILES['clientLogo']) &&
//    $_FILES['clientLogo']['error'] == UPLOAD_ERR_OK) {
//    $mail->AddAttachment($_FILES['clientLogo']['tmp_name'],
//        $_FILES['clientLogo']['name']);
//}

if (!$template)
{
    echo 'Please select preferred template';
    exit;
}

if (!$clientName)
{
    echo 'Please enter clients name';
    exit;
}

if (!$clientUrl)
{
    echo 'Please enter clients URL';
    exit;
}
elseif (!filter_var($clientUrl, FILTER_VALIDATE_URL))
{
    echo 'URL is not valid';
    exit;
}

if (!$hrBossContactName)
{
    echo 'Please enter HR boss contact name';
    exit;
}

if (!$hrBossEmail)
{
    echo 'Please enter HR boss contact email';
    exit;
}
elseif (!filter_var($hrBossEmail, FILTER_VALIDATE_EMAIL))
{
    echo 'Invalid email format';
    exit;
}

if (!$hrBossPhone)
{
    echo 'Please enter a telephone number';
    exit;
}
elseif (!is_numeric($hrBossPhone) || strlen($hrBossPhone) < 10)
{
    echo 'Please enter a valid Tel Number';
    exit;
}

if (!$requiredFromClient)
{
    echo 'Please enter details for required from client';
    exit;
}

if (!$clientFont)
{
    echo 'Please advise on client font';
    exit;
}

if (!$primaryColor)
{
    echo 'Please advise on client primary brand colour';
    exit;
}

if (!$selectLanguage)
{
    echo 'Please select language for portal';
    exit;
}

$from        = new stdClass();
$from->name  = 'JXT';
$from->email = 'no-reply@jxt.com.au';

$subject = 'JXT HRBoss - Site Request';

/* ---- first email ----- */

$to        = new stdClass();
$to->name  = 'Neil Kearney';
$to->email = 'mail@neilkearney.net';

$mail_message = "<img src='http://www.jxt.com.au/themes/jxt-2012/v2/img/jxtConsultingLogo.png'/> <br><br>
  <p style='font-family:helvetica; font-size:18px; line-height:20px; '>An enquiry has been made.</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Template selected: {$template}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client name: {$clientName}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client Url: {$clientUrl}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>HR Boss Contact name: {$hrBossContactName}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>HR Boss Email: {$hrBossEmail}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>HR Boss Phone: {$hrBossPhone}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Required from client: {$requiredFromClient}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client font: {$clientFont}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client primary color: {$primaryColor}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client secondary color: {$secondaryColor}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Branding Guidelines: {$brandingGuidelines}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Preferred language: {$selectLanguage}</p>";

$result1 = send_email($to, $subject, $mail_message, $from);

/* ---- second email ----- */

$to        = new stdClass();
$to->name  = 'Neil Kearney';
$to->email = 'neil@jxt.com.au';

$mail_message = "<img src='http://www.jxt.com.au/themes/jxt-2012/v2/img/jxtConsultingLogo.png'/> <br><br>
  <p style='font-family:helvetica; font-size:18px; line-height:20px; '>An enquiry has been made.</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Template selected: {$template}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client name: {$clientName}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Client Url: {$clientUrl}</p>";

$result2 = send_email($to, $subject, $mail_message, $from);

if ($result1 === true && $result2 === true)
{
    echo 1;
}
