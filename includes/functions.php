<?php

function send_email($to, $subject, $message, $from = null)
{
    require_once 'phpmailer/class.phpmailer.php';

    $mail = new PHPMailer();

    $mail->IsSMTP();

    $mail->Host = "smtp.sendgrid.net";
    $mail->Port = 465;
    $mail->SMTPSecure = "ssl";
    $mail->SMTPAuth = true;
    $mail->Username = "support.jxt";
    $mail->Password = "ginger9498";

    $mail->Subject = $subject;

    $mail->MsgHTML($message);

    $mail->AddAddress($to->email, $to->name);

    if ($from)
    {
        $mail->SetFrom($from->email, $from->name);
    }

    if (!$mail->Send())
    {
        return $mail->ErrorInfo;
    }
    else
    {
        return true;
    }
}

function send_test_email()
{
    $to = new stdClass();
    $to->name = 'Amit Kumar';
    $to->email = 'amit@jxt.com.au';

    $from = new stdClass();
    $from->name = 'Salary Spy';
    $from->email = 'no-reply@salaryspy.com.au';

    $subject = 'Test';

    $message = 'This is a test message';

    $result = send_email($to, $subject, $message, $from);

    if ($result !== true)
    {
        echo $result;
    }
    else
    {
        echo 'Sent';
    }
}

?>
