// Start of porfolio sandbox script ///////////////////////

jQuery(document).ready(function()
{
	var destination_list = jQuery('#list');

	var source_list = destination_list.clone();

	jQuery('#filter a').click(function(event)
	{
		event.preventDefault();
		var value = jQuery(this).attr('data-value');
		var items;
		if(value=='all')
		{
			items = source_list.find('li');
		}
		else
		{
			items = source_list.find('li.' + value);
		}

		destination_list.quicksand(items);
		jQuery('#filter li').removeClass('active');
		jQuery(this).closest('li').addClass('active');
	});


});


$('a[data-href]').click(function()
{
	var target = $(this).attr('data-href');

	target = $('#' + target);

	if (target.length)
	{
		$('html,body').animate(
		{
			scrollTop: target.offset().top - 30
		}, 1000);
	}

	return false;
});


jQuery(document).ready(function() {
	var offset = 220;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset) {
			jQuery('.back-to-top').fadeIn(duration);
		} else {
			jQuery('.back-to-top').fadeOut(duration);
		}
	});

	jQuery('.back-to-top').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	})
});

// end of scroll to script ///////////////////////


function submitForm()
{
	var form = jQuery('#form');

	form.submit();
}

function submitForm2(el)
{
	var form = jQuery(el).closest('form');

	if(form.length==0)
	{
		return;
	}

    form.find('.btn').attr('disabled', 'disabled');

	jQuery.ajax(
	{
		type: "POST",
		url: form.attr("action"),
		cache: false,
		dataType: "html",
		data: form.serialize()
	})
	.done(function( response )
	{
		response = jQuery.trim(response);

		if(response!='1')
		{
			alert(response);

			return;
		}

		var msg = form.attr('data-msg-success');

		if(msg && msg!='')
		{
			alert(msg);
		}
		else
		{
			alert('Your request was processed successfully.');
		}

		form[0].reset();
	})
	.fail(function(jqXHR, textStatus)
	{
		alert('An error occurred while processing your request.');
	})
	.complete(function()
	{
        form.find('.btn').removeAttr('disabled');
	});
}

$(".reset").click(function() {
	$(this).closest('form').find("input[type=text], input[type=password], textarea").val("");
});