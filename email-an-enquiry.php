<?php

require_once 'includes/functions.php';

$name = isset($_REQUEST['name']) ? trim($_REQUEST['name']) : NULL;
$phone = isset($_REQUEST['phone']) ? trim($_REQUEST['phone']) : NULL;
$email = isset($_REQUEST['email']) ? trim($_REQUEST['email']) : NULL;
$company = isset($_REQUEST['company']) ? trim($_REQUEST['company']) : NULL;
$website = isset($_REQUEST['website']) ? trim($_REQUEST['website']) : NULL;
$message = isset($_REQUEST['message']) ? trim($_REQUEST['message']) : NULL;

if (!$name)
{
    echo 'Please enter your full name';

    exit;
}

if (!$phone)
{
    echo 'Please enter a telephone number to callback';

    exit;
}
elseif (!is_numeric($phone) || strlen($phone) < 10)
{
    echo 'Please enter a valid Tel Number';

    exit;
}

if (!$email)
{
    echo 'Please enter your email address';

    exit;
}
elseif (!filter_var($email, FILTER_VALIDATE_EMAIL))
{
    echo "E-mail is not valid";

    exit;
}

if (!$message)
{
    echo 'Please enter your enquiry';

    exit;
}

$message = str_replace("\n", '<br />', $message);

$to = new stdClass();
$to->name = 'Neil Kearney';
$to->email = 'neil@jxt.com.au';

$subject = 'JXT Broadbean - Email an Enquiry';

$mail_message = "<img src='http://www.jxt.com.au/themes/jxt-2012/v2/img/jxtConsultingLogo.png'/> <br><br>
  <p style='font-family:helvetica; font-size:18px; line-height:20px; '>An enquiry has been made.</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Name: {$name}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Company: {$company}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Website: {$website}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Email: {$email}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Phone: {$phone}</p>
  <p style='font-family:helvetica; font-size:14px; line-height:16px; '>Message: {$message}</p>";


$from = new stdClass();
$from->name = 'JXT';
$from->email = 'no-reply@jxt.com.au';

$result = send_email($to, $subject, $mail_message, $from);

if ($result === true)
{
    echo 1;
}
?>
