<?php

$submit_url = 'http://rnd.jxt.com.au/sitecheck/create';

$email_recipients = array();

$error_codes = array();
$error_codes[-1] = 'An error occured while processing your request';
$error_codes[1] = 'Name is required';
$error_codes[2] = 'Email address is required';
$error_codes[3] = 'Website address is required';
$error_codes[4] = 'Mobile is required';

$success_codes = array();
$success_codes[-1] = 'Your request was submitted successfully. We will get back to you soon.';
?>