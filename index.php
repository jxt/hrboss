<!DOCTYPE html>

<html lang="en">
    <head>
        <title>JXT HRBoss Page</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/carousal.css" rel="stylesheet">
        <link href="css/prettyPhoto.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css'>
        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="http://www.jxt.com.au/themes/jxt-2012/v2/img/touch-icon-iphone.png">
    </head>

    <body>
        <section class="top-nav1">
            <div class="row clearfix tophead-spacing">

                <div class="col-xs-12 col-md-12 column">
                    <nav class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <a class="jxt-logo" href="http://www.jxt.com.au">JXT Logo</a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right top-bb-links">
                                <li>
                                    <a href="http://www.jxt.com.au"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp; Home</a>
                                </li>
                                <li>
                                    <a href="#" data-href="whoAnchor">Who is JXT</a>
                                </li>
                                <li>
                                    <a href="#" data-href="GetTouchAnchor" >Get in Touch</a>
                                </li>

                                <li>
                                    <a href="#" data-href="GetTouchAnchor">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </section>

        <section class="broadbean-slider">
            <!-- Carousel ================================================== -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <ol class="col-xs-12 carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1" ></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="img/hrboss-bannerb1.png" alt="first Slide" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>HRBoss B1 template</h1>
                                <p>The new B1 template now available. Click for preview</p>
                                <p><a class="btn btn-lg orange-btn" href="http://b1.jxt.com.au.jxt1.com" role="button" target="_blank">View template</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/hrboss-bannerb2.png" alt="Second Slide" class="img-responsive">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>HRBoss B2 template</h1>
                                <p>The new B2 template now available. Click for preview</p>
                                <p><a class="btn btn-lg orange-btn" href="http://b2.jxt.com.au.jxt1.com" target="_blank" role="button">View template</a></p>
                            </div>
                        </div>
                    </div>


                </div>

                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div><!-- /.carousel -->
        </section>

        <section class="whoIs">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 column">
                        <h1 class="text-center no-top-margin" id="whoAnchor">
                            WHO IS JXT?
                        </h1>

                        <p class="lead text-center">
                            JXT simplifies marketing opportunities for employers and recruiters in the digital world.
                        </p>

                        <img src="img/splitter.png" alt="splitter image" class="img-responsive centre-images">
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-6 column">
                        <h2>
                            The result?
                        </h2>

                        <p class="text-left lead">
                            A stronger brand awareness and reputation; a better ROI on the investment in candidate engagement; happier candidates; better placed hires.
                        </p>
                    </div>
                    <div class="col-xs-12 col-md-6 column">
                        <div class="panel-group" id="panel-513972">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a class="panel-title" data-toggle="collapse" data-parent="#panel-513972" href="#panel-element-649017">Thanks to us....</a>
                                </div>

                                <div id="panel-element-649017" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li>257 recruitment consultants in Australia run online recruitment activity that’s best practice</li>
                                            <li>824 clients are using clever social media apps for finding talent</li>
                                            <li>756 social media profiles are engaging with talent in ways that are memorable, clever &amp; fun</li>
                                            <li>311 organisations in Australia have red-hot hiring websites</li>
                                            <li>more job boards and hiring websites are powered through our job board software than any other in Australia</li>
                                            <li>innumerable candidates have had much better hiring experiences than ever before</li>
                                            <li>more clients see the hiring process as a strategic business function, and so commit to a better process: for their managers, their staff, their candidates and their clients</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a class="panel-title" data-toggle="collapse" data-parent="#panel-513972" href="#panel-element-705255">....what else you need to know?</a>
                                </div>

                                <div id="panel-element-705255" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>We saw a gap in the market, back in 2006 (light years in social media time). Where no really cool, savvy digital recruitment solutions existed. Solutions that recognised the changes in the media landscape: from monologue to dialogue. So we started to build clever strategies for recruiters &amp; hiring brands to help them reach talent in high-traffic, online environments.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="getInTouch">
            <div class="container">
                <div class="row clearfix" >
                    <div class="col-xs-12 col-md-12 column">
                        <h1 id="GetTouchAnchor">
                            Site request
                        </h1>
                    </div>
                </div>

                <form action="hrboss.php" method="POST" data-msg-success="Your enquiry has been successfully submitted. We will get back to you as soon as possible. Thank you." onsubmit="return false;">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control" name="selectTemplate">
                                    <option value="">Please select template</option>
                                    <option value="b1 template">B1 template</option>
                                    <option value="b2 template">B2 template</option>
                                    <option value="Enterprise">Enterprise</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Client name" name="clientName" value="" required>
                            </div>

                            <div class="form-group">
                                <input type="text" placeholder="Client Url" class="form-control" name="clientUrl" value="" required>
                            </div>

                            <div class="form-group">
                                <input type="text" placeholder="HR Boss Contact name" class="form-control" name="hrBossContactName" value="" required>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="HR Boss Email" class="form-control" name="hrBossEmail" value="" required>
                            </div>


                            <div class="form-group">
                                <input type="text" placeholder="HR Boss Phone Number" class="form-control" name="hrBossPhone" value="" required>
                            </div>

                            <div class="form-group">
                                <textarea placeholder="Required from the client" class="form-control" name="requiredFromClient" rows="5" required></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="Company" type="text" placeholder="Client font" name="clientFont" />
                                <p class="help-block">Provide name or link of client font</p>
                            </div>

                            <div class="form-group">
                                <input class="form-control" id="Website" type="text" placeholder="Primary Corporate colour:" name="primaryColor" required/>
                                <p class="help-block">Provide HEX or RGB reference</p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="Website" type="text" placeholder="Secondary Corporate colour:" name="secondaryColor" />
                                <p class="help-block">Provide HEX or RGB reference</p>
                            </div>

                            <div class="form-group">
                                <textarea placeholder="Branding guidelines" class="form-control" name="brandingGuidelines" rows="5"  required></textarea>
                            </div>

                            <div class="form-group">
                                <select name="selectLanguage" id="" class="form-control" required>
                                    <option value="">Please select language</option>
                                    <option value="English">English</option>
                                    <option value="Chinese">Chinese</option>
                                    <option value="Japanese">Japanese</option>
                                    <option value="Other">Other</option>
                                </select>
                                <p class="help-block">If other, please advise language requirement in requirements section.</p>
                            </div>
    <!--                        <div class="form-group">-->
    <!--                            <label for="clientLogo">Client logo</label>-->
    <!--                            <input type="file" required name="clientLogo"/>-->
    <!--                            <p class="help-block">Provide eps/ai file</p>-->
    <!--                        </div>-->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        <div class="from-group">
                            <button type="button" class="SearchButton btn btn-primary" onclick="submitForm2(this);">Submit Check</button>
                        </div>
                        </div>
                    </div>
                </form>

                <?php
                if (!empty($error_code))
                {
                    echo '<div class="error">';

                    foreach ($error_code as $ec)
                    {
                        echo $error_codes[$ec], '<br />';
                    }

                    echo '</div>';
                }

                if (!empty($success_code))
                {
                    echo '<div class="success">';

                    foreach ($success_code as $sc)
                    {
                        echo $success_codes[$sc], '<br />';
                    }

                    echo '</div>';
                }
                ?>
            </div>
        </section>


        <section class="footer-section">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-7">
                        <ul class="socialLinks">
                            <li><a href="http://www.linkedin.com/company/402565" rel="tooltip" id="linkedIn" target="_blank" data-toggle="tooltip" title="" data-original-title="Connect with us on LinkedIn">linkedIn</a></li>

                            <li><a href="https://www.facebook.com/jxtconsulting" rel="tooltip" id="facebook" target="_blank" title="" data-original-title="Connect with us on Facebook">facebook</a></li>

                            <li><a href="https://twitter.com/jxtconsulting" rel="tooltip" id="twitter" target="_blank" title="" data-original-title="Connect with us on Twitter">twitter</a></li>

                            <li><a href="https://plus.google.com/u/0/117803545067659033842/posts" rel="tooltip" id="gplus" target="_blank" title="" data-original-title="Connect with us on Google+">google+</a></li>

                            <li><a href="http://www.youtube.com/jxtmediacentre" rel="tooltip" id="youTube" target="_blank" title="" data-original-title="Connect with us on Youtube">youtube</a></li>

                            <li><a href="http://pinterest.com/jxtconsulting/" rel="tooltip" id="pinterest" target="_blank" title="" data-original-title="Connect with us on Pinterest">pinterest</a></li>

                            <li><a href="http://instagram.com/jxtconsulting" rel="tooltip" id="instagram" target="_blank" title="" data-original-title="Connect with us on Instagram">instagram</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-md-5 pull-right"><small class="copyright-links"><a href="http://www.jxt.com.au/page/terms-and-conditions">TERMS &amp; CONDITIONS</a> <a href="http://www.jxt.com.au/page/privacy-policy">PRIVACY</a> <a href="http://support.jxt.com.au/support/home" target="_blank">SUPPORT</a> <br> COPYRIGHT © <a href="http://www.jxt.com.au" target="_blank">JXT</a> 2013. ALL RIGHTS RESERVED.</small></div>
                </div>
            </div>
        </section>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/modernizr.js"></script>
        <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="js/jquery.quicksand.js"></script>
        <script type"text/javascript"  src="js/jquery.easing.min.js"></script>
        <script type"text/javascript"  src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>

        <a href="#top" class="back-to-top">Back to Top</a>
    </body>
</html>
